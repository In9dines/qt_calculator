#include <QMainWindow>
#include <QLineEdit>
#include <exception>

class CalculatorMainWindow : public QMainWindow
{
Q_OBJECT

public:
    CalculatorMainWindow(QWidget *parent = nullptr) : QMainWindow(parent) {}

    QLineEdit *lineEdit = nullptr;
    QLineEdit *lineEdit_2 = nullptr;
    QLineEdit *lineEdit_3 = nullptr;
public slots:
    void addPlus()
    {
        lineEdit_3->setText(QString::number(lineEdit->text().toDouble() + lineEdit_2->text().toDouble()));
    }
    void addMinus()
    {
        lineEdit_3->setText(QString::number(lineEdit->text().toDouble() - lineEdit_2->text().toDouble()));
    }
    void addMultiply()
    {
        lineEdit_3->setText(QString::number(lineEdit->text().toDouble() * lineEdit_2->text().toDouble()));
    }
    void addDivide()
    {
        try
        {
            lineEdit_3->setText(QString::number(lineEdit->text().toDouble() / lineEdit_2->text().toDouble()));;
        }
        catch(...)
        {
            lineEdit_3->setText("ERROR");
        }
    }
};