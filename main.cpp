#include <QApplication>
#include "./ui_calculator.h"
#include "calculatorMainWindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CalculatorMainWindow window(nullptr);
    Ui::MainWindow calculator;
    calculator.setupUi(&window);
    calculator.lineEdit = window.lineEdit;
    calculator.lineEdit_2 = window.lineEdit_2;
    calculator.lineEdit_3 = window.lineEdit_3;
    window.show();
    return QApplication::exec();
}